const socketIo = require('socket.io')
 
let io;

function initsocket(server){
 io = socketIo(server)

 io.on('connection',(socket)=>{
    console.log("new user connected");
    io.emit('welcome', "welcome to socket chat "+socket.id)
})
}

function getSocket(){
    if (!io){
        throw new Error('crashed')
    }
    return io
}
module.exports = {
    initsocket,
    getSocket
}